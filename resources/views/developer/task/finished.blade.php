@extends('layouts.developer.app')

@section('page_title', 'Projects')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .btn-sm{
            padding: 5px 7px;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>
            Finished Tasks <br>
            <span class="font-size-12">All the Tasks I was assigned and that has been <b class="text-danger">Finished.</b></span>
        </h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-block">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">Sl.</th>
                                    <th class="text-center">Task Name</th>
                                    <th class="text-center">Project Name</th>
                                    <th class="text-center">Priority</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Starting Date</th>
                                    <th class="text-center">Ending Date</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>01</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span>
                                                <b><a href="{{ route('developer.task.show', ['developer_id'=> 1, 'task_id' => 1]) }}" class="text-info">Task_Name</a></b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span>
                                                <b><a href="{{ route('developer.project.show', ['developer_id'=> 1, 'project_id' => 1]) }}" class="text-info">Project_Name</a></b>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15 text-center">
                                            <span class="text-dark">
                                                <b>Low</b>
                                                {{-- <b>Medium</b>
                                                <b>High</b>
                                                <b>Urgent</b> --}}
                                            </span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="relative mrg-top-15 text-center">
                                            <span class="text-success"><b>Completed</b></span>
                                        </div>
                                        {{-- <div class="relative mrg-top-15">
                                            <span class="text-warning"><b>Processing</b></span>
                                        </div>
                                        <div class="relative mrg-top-15">
                                            <span class="text-danger"><b>Canceled</b></span>
                                        </div> --}}
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span>6 Nov 2019</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <span>6 Dec 2019</span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <b class="text-dark font-size-16">£168.00</b>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="mrg-top-15">
                                            <a href="{{ route('developer.task.show', ['developer_id'=> 1, 'task_id' => 1]) }}" class="btn btn-outline-info btn-icon btn-rounded btn-sm">
                                                <i class="ti-eye text-bold"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

    </script>
@endsection
