@extends('layouts.developer.app')

@section('page_title', 'Support | Contact')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .note-fontname .note-btn{
            min-width: 15vw;
        }
        .note-view{
            display: none;
        }
        .form-control {
            padding: .600rem 0.75rem;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 10px;
            border-bottom: 1px solid #ededed;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>Contact Message</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <form action="#" method="post">
                        @csrf
                        <div class="card">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="mrg-bottom-10 text-dark"><b>Receiver <span class="required">*</span></b></label>
                                        <select class="@error('mail_to') is-invalid @enderror" name="mail_to" id="selectize-dropdown" multiple required>
                                            <option value="name_one">name_one</option>
                                            <option value="name_two">name_two</option>
                                            <option value="name_three">name_three</option>
                                        </select>

                                        @error('mail_to')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="mrg-bottom-10 text-dark"><b>Subject <span class="required">*</span></b></label>
                                        <input type="text" name="subject" class="form-control input-md @error('subject') is-invalid @enderror" value="{{ old('subject') }}" placeholder="Artist Website" autocomplete="off" required>

                                        @error('subject')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="mrg-bottom-10 text-dark"><b>Your Message <span class="required">*</span></b></label>
                                        <textarea id="summernote-usage" name="details" class="form-control @error('details') is-invalid @enderror" placeholder="mm/dd/yyyy" required>{{ old('details') }}</textarea>

                                        @error('details')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer border top">
                                <div class="text-right">
                                    <button type="submit" class="btn btn-dark text-bold">SEND MESSAGE</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script src="{{ asset('assets/js/apps/email.js') }}"></script>
    <script>
        // Custom Script Here

    </script>
@endsection
