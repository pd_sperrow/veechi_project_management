<div class="modal slide-in-right modal-right fade " id="add_finance">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4>New Finance</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="#" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Role <span class="required">*</span></label>
                                    <input type="text" name="role_name" readonly class="form-control" value="Finance" disabled>
                                    <input type="hidden" name="role_id" value="3" required>
                                </div>
                                <div class="form-group">
                                    <label>Full Name <span class="required">*</span></label>
                                    <input type="text" name="name" class="form-control" placeholder="Ahad Ullah Shah" required>
                                </div>
                                <div class="form-group">
                                    <label>Email Address <span class="required">*</span></label>
                                    <input type="email" name="email" class="form-control" placeholder="ahad@veechitechnologies.com" required>
                                </div>
                                <div class="form-group">
                                    <label>Contact No. <span class="required">*</span></label>
                                    <input type="text" name="contact_number" class="form-control" placeholder="+44 7542 987483" required>
                                </div>
                                <div class="form-group">
                                    <label>Password. <span class="required">*</span> <a href="#" class="password-generate text-danger" id="passwordGenerate">Generate</a></label>
                                    <input type="text" name="password" id="password_generator" class="form-control" placeholder="123ABCabc!@#$%" required>
                                </div>
                                <div class="checkbox checkbox-danger font-size-12">
                                    <input id="make_superadmin" name="make_superadmin" type="checkbox" required>
                                    <label for="make_superadmin">Make Finance</label>
                                </div>
                                <div class="checkbox checkbox-danger font-size-12">
                                    <input id="send_mail" name="send_mail" type="checkbox">
                                    <label for="send_mail">Send Mail With Login Informations</label>
                                </div>
                                <button class="btn btn-danger btn-sm btn-block" type="submit">Assign As <span class="text-bold">Finance</span></button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
