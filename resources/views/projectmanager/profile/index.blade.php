@extends('layouts.projectmanager.app')

@section('page_title', 'Profile')

@section('css_links')
    {{--  External CSS  --}}

@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .form-control {
            padding: 0.700rem 0.75rem;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>{{ $profile->name }}</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="widget-profile-1 card">
                        <div class="profile border bottom">
                            <img class="mrg-top-30" src="{{ asset('assets/images/others/img-10.jpg') }}" alt="">
                            <h4 class="mrg-top-20 no-mrg-btm text-semibold">{{ $profile->name }}</h4>
                            <p>PROJECT MANAGER</p>
                        </div>
                        <div class="pdd-horizon-30 pdd-vertical-20">
                            <div class="mrg-top-10 text-center">
                                <ul class="list-unstyled list-inline">
                                    <li class="list-inline-item no-pdd-horizon">
                                        <a href="#" class="btn btn-facebook btn-icon btn-rounded">
                                            <i class="ti-facebook"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item no-pdd-horizon">
                                        <a href="#" class="btn btn-linkedin btn-icon btn-rounded">
                                            <i class="ti-linkedin"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item no-pdd-horizon">
                                        <a href="#" class="btn btn-skype btn-icon btn-rounded">
                                            <i class="ti-skype"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <form action="{{ route('common_auth.profile.update.password') }}" method="post">
                        @csrf
                            <div class="card-heading border bottom">
                                <h4 class="card-title">Authentication</h4>
                            </div>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="mrg-bottom-10 text-dark"><b>Old Password</b></label>
                                        <input type="password" name="old_password" class="form-control @error('old_password') is-invalid @enderror" placeholder="Old Password" autocomplete="off" required>

                                        @error('old_password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="mrg-bottom-10 text-dark"><b>New Password</b></label>
                                        <input type="password" name="new_password" class="form-control @error('new_password') is-invalid @enderror" placeholder="New Password" autocomplete="off" required>

                                        @error('new_password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row mrg-top-15">
                                    <div class="col-md-12">
                                        <label class="mrg-bottom-10 text-dark"><b>Confirm New Password</b></label>
                                        <input type="password" name="confirm_password" class="form-control @error('confirm_password') is-invalid @enderror" placeholder="Confirm New Password" autocomplete="off" required>

                                        @error('confirm_password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer border top">
                                <div class="text-right">
                                    <button type="submit" class="btn btn-dark btn-sm">Update Password</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-8">
                    <form action="#" method="post">
                        @csrf
                        <div class="card">
                            <div class="card-heading border bottom">
                                <h4 class="card-title">General Info</h4>
                            </div>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="mrg-top-10 text-dark"> <b>Avatar</b></p>
                                    </div>
                                    <div class="col-md-6">
                                        <div>
                                            <label for="img-upload" class="pointer">
                                                <img src="{{ asset('assets/images/others/img-10.jpg') }}" width="117" alt="">
                                                <span class="btn btn-default display-block no-mrg-btm">Choose file</span>
                                                <input class="d-none @error('avatar') is-invalid @enderror" type="file" name="avatar" multiple id="img-upload">
                                            </label>
                                            @error('avatar')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="mrg-top-10 text-dark"> <b>Name</b></p>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ $profile->name }}" placeholder="Mr. Ahad Ullah Shah" autocomplete="off" required>

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="mrg-top-10 text-dark"> <b>Primary Email</b></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="mrg-top-10">{{ $profile->email }}</p>
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="mrg-top-10 text-dark"> <b>Contact Email</b></p>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="contact_email" class="form-control @error('contact_email') is-invalid @enderror" value="{{ $profile->contact_email }}" placeholder="contact@veechitechnologies.com" autocomplete="off" required>

                                        @error('contact_email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="mrg-top-10 text-dark"> <b>Contact No</b></p>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="contact_no" class="form-control @error('contact_no') is-invalid @enderror" value="{{ $profile->contact_no }}" placeholder="+44 1234 567890" autocomplete="off" required>

                                        @error('contact_no')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="mrg-top-10 text-dark"> <b>Whatsapp No</b></p>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="whatsapp_no" class="form-control @error('whatsapp_no') is-invalid @enderror" value="{{ $profile->whatsapp_no }}" placeholder="+44 1234 567890" autocomplete="off" required>

                                        @error('whatsapp_no')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="mrg-top-10 text-dark"> <b>Address</b></p>
                                    </div>
                                    <div class="col-md-6">
                                    <textarea name="address" class="form-control @error('address') is-invalid @enderror" rows="3" placeholder="New York, United State" autocomplete="off" required>{{ $profile->address }}</textarea>

                                        @error('address')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="mrg-top-10 text-dark"> <b>Facebook</b></p>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="url" name="facebook" class="form-control @error('facebook') is-invalid @enderror" value="{{ $profile->facebook }}" placeholder="https://www.facebook.com/VeechiTechnologies/" autocomplete="off">

                                        @error('facebook')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="mrg-top-10 text-dark"> <b>Linkedin</b></p>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="url" name="linkedin" class="form-control @error('linkedin') is-invalid @enderror" value="{{ $profile->linkedin }}" placeholder="https://www.linkedin.com/company/veechi-technologies" autocomplete="off">

                                        @error('linkedin')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-3">
                                        <p class="mrg-top-10 text-dark"> <b>Skype</b></p>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="skype" class="form-control @error('skype') is-invalid @enderror" value="{{ $profile->skype }}" placeholder="veechi.technologies" autocomplete="off">

                                        @error('skype')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer border top">
                                <div class="text-right">
                                    <button type="submit" class="btn btn-dark btn-sm">Update Profile</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here
    </script>
@endsection
