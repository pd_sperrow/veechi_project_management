@extends('layouts.client.app')

@section('page_title', 'My Projects | New Project')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .form-control {
            padding: 0.548rem 0.75rem;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>Apply For A New Project</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ route('client.project.store') }}" method="post">
                        @csrf
                        <div class="card">
                            <div class="card-heading border bottom">
                                <h4 class="card-title">Project Description</h4>
                            </div>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="mrg-bottom-10 text-dark"><b>Project Name <span class="required">*</span></b></label>
                                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="Artist Website" autocomplete="off" required>

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="col-md-4">
                                        <label class="mrg-bottom-10 text-dark"><b>Project Category <span class="required">*</span></b></label>
                                        <select class="@error('category') is-invalid @enderror" name="category" id="selectize-dropdown" required>
                                            <option disabled selected>Select a Category</option>
                                            <option value="Website">Website</option>
                                            <option value="Software">Software</option>
                                        </select>

                                        @error('category')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="col-md-4">
                                        <label class="mrg-bottom-10 text-dark"><b>Approximate Deadline</b></label>
                                        <input type="text" name="deadline" class="form-control datepicker-1 @error('deadline') is-invalid @enderror" value="{{ old('deadline') }}" placeholder="mm/dd/yyyy" data-provide="datepicker" autocomplete="off">

                                        @error('deadline')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="mrg-bottom-10 text-dark"><b>Project Details <span class="required">*</span></b></label>
                                        <textarea id="summernote-usage" name="details" class="form-control @error('details') is-invalid @enderror" placeholder="mm/dd/yyyy" required>{{ old('details') }}</textarea>

                                        @error('details')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer border top">
                                <div class="text-right">
                                    <button type="submit" class="btn btn-dark btn-sm">Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

    </script>
@endsection
