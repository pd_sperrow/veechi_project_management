@extends('layouts.client.app')

@section('page_title', 'Invoices')

@section('css_links')
    {{--  External CSS  --}}

@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        a{
            text-decoration: none !important;
        }
        table.table.table-borderless tbody tr th,
        table.table.table-borderless tbody tr td{
            line-height: 0.5;
            border-color: #ffffff;
        }
        .border-top-1{
            border-top: 1px dashed #dddddd !important;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="container">
        <div class="card">
            <div class="pdd-vertical-5 pdd-horizon-10 border bottom print-invisible">
                <ul class="list-unstyle list-inline text-right">
                    <li class="list-inline-item">
                        <a href="#" class="btn text-gray text-hover display-block padding-10 no-mrg-btm" onclick="window.print();">
                            <i class="ti-printer text-info pdd-right-5"></i>
                            <b>Print</b>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#" class="text-gray text-hover display-block padding-10 no-mrg-btm">
                            <i class="fa fa-file-pdf-o text-danger pdd-right-5"></i>
                            <b>Export PDF</b>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="pdd-horizon-30">
                    <div class="mrg-top-15">
                        <div class="inline-block">
                            <img class="img-responsive" src="{{ asset('assets/images/logo/logo.png') }}" alt="">
                            <address class="pdd-left-10 mrg-top-20">
                                <b class="text-dark">Veechi Technologies LTD</b><br>
                                <span>8 Rochdale Road</span><br>
                                <span>Royton, England</span><br>
                                <span>OL2 6QJ</span><br>
                            </address>
                        </div>
                        <div class="pull-right">
                            <h2>
                                <b>
                                    INVOICE:
                                    <a href="#">VT-INV-000001</a>
                                </b>
                            </h2>
                        </div>
                    </div>
                    <div class="row mrg-top-20">
                        <div class="col-md-9 col-sm-9">
                            <h3 class="pdd-left-10 mrg-top-10">Invoice To:</h3>
                            <address class="pdd-left-10 mrg-top-10">
                                <b class="text-dark">
                                    <a href="#" class="customer-name">Mr. Client Name</a>
                                </b><br>
                                <span>8626 Maiden Dr. </span><br>
                                <span>Niagara Falls, New York 14304</span>
                            </address>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="mrg-top-50">
                                <div class="text-dark text-uppercase inline-block"><b>Invoice No :</b></div>
                                <b><a href="#" class="pull-right">VT-INV-000001</a></b>
                            </div>
                            <div class="mrg-top-0">
                                <div class="text-dark text-uppercase inline-block">
                                    <b>Invoice Date :</b>
                                </div>
                                <div class="pull-right">17/11/2019</div>
                            </div>
                            <div class="mrg-top-0">
                                <div class="text-dark text-uppercase inline-block">
                                    <b>Due Date :</b>
                                </div>
                                <div class="pull-right">25/11/2019</div>
                            </div>
                        </div>
                    </div>
                    <div class="row mrg-top-20">
                        <div class="col-md-12">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Sl.</th>
                                        <th>Items</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Discount</th>
                                        <th class="text-right">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>01</td>
                                        <td>5 Page eCommerce Website</td>
                                        <td>1</td>
                                        <td>£450.00</td>
                                        <td>10%</td>
                                        <td class="text-right">£405.00</td>
                                    </tr>
                                    <tr>
                                        <td>02</td>
                                        <td>5GB Hosting</td>
                                        <td>1</td>
                                        <td>£100.00</td>
                                        <td>0%</td>
                                        <td class="text-right">£100.00</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="row mrg-top-30">
                                <div class="col-md-4 offset-md-8">
                                    <hr>
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <th class="text-right"><b>Sub Total:</b></th>
                                                <td class="text-right"><b>£550.00</b></td>
                                            </tr>
                                            <tr>
                                                <th class="text-right"><b>Discount:</b></th>
                                                <td class="text-right"><b>£45.00</b></td>
                                            </tr>
                                            <tr class="text-info">
                                                <th class="text-right border-top-1">
                                                    <b>Total Amount To Pay:</b>
                                                </th>
                                                <td class="text-right border-top-1"><b>£505.00</b></td>
                                            </tr>
                                            <tr class="text-success">
                                                <th class="text-right border-top-1">
                                                    <b>Total Paid:</b>
                                                </th>
                                                <td class="text-right border-top-1"><b>£300.00</b></td>
                                            </tr>
                                            <tr class="text-danger">
                                                <th class="text-right border-top-1">
                                                    <b>Due Amount:</b>
                                                </th>
                                                <td class="text-right border-top-1"><b>£205.00</b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                </div>
                            </div>
                            <div class="row mrg-top-30">
                                <div class="col-md-12">
                                    <hr>
                                    <div class="border top bottom pdd-vertical-20">
                                        <p class="text-dark">
                                            [[ <b class="text-info">NOTE:</b>
                                            <b>If you choose bank payment then use the bank details below...</b> ]]
                                        </p>

                                        <address class="pdd-left-10 mrg-top-10">
                                            <b class="text-dark">Veechi Technologies LTD</b><br>
                                            <span class="text-info"><b class="text-dark">SC:</b> 80-22-60</span><br>
                                            <span class="text-info"><b class="text-dark">Account:</b> 18003665</span><br>
                                            <span class="text-info"><b class="text-dark">Ref:</b> VT-INV-000001</span>
                                        </address>
                                    </div>
                                </div>
                            </div>
                            <div class="row mrg-vertical-20">
                                <div class="col-md-6">
                                    <img class="img-responsive text-opacity mrg-top-0" width="150" src="{{ asset('assets/images/logo/logo.png') }}" alt="">
                                </div>
                                <div class="col-md-6 text-right">
                                    <small>
                                        <b>
                                            <a href="https://www.veechitechnologies.com" target="_blank">www.veechitechnologies.com</a>
                                        </b>
                                    </small><br>
                                    <small><b>Phone:</b> (123) 456-7890</small>
                                    <br>
                                    <small>info@veechitechnologies.com</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

    </script>
@endsection
