<div class="header navbar">
    <div class="header-container">
        <ul class="nav-left">
            <li>
                <a class="side-nav-toggle" href="javascript:void(0);">
                    <i class="ti-view-grid"></i>
                </a>
            </li>
            <li class="search-box">
                <a class="search-toggle no-pdd-right" href="javascript:void(0);">
                    <i class="search-icon ti-search pdd-right-10"></i>
                    <i class="search-icon-close ti-close pdd-right-10"></i>
                </a>
            </li>
            <li class="search-input">
                <input class="form-control" type="text" placeholder="Search...">
            </li>
        </ul>
        <ul class="nav-right">
            {{-- Notifications --}}
            <li class="notifications dropdown pdd-right-10" style="border-right: 1px solid #efefef;">
                <span class="counter mrg-right-10">00</span>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="ti-bell"></i>
                </a>

                <ul class="dropdown-menu">
                    <li class="notice-header">
                        <i class="ti-bell pdd-right-10"></i>
                        <span>Notifications</span>
                    </li>
                    <li>
                        <ul class="list-info overflow-y-auto relative scrollable ps-container ps-theme-default ps-active-y" data-ps-id="8964a8c7-94b9-3a1f-b6b9-c6f2dc7d257c">
                            <li>
                                <a href="#">
                                    <img class="thumb-img" src="{{ asset('assets/images/avatars/thumb-4.jpg') }}" alt="">
                                    <div class="info">
                                        <span class="title">
                                            <span class="font-size-14 text-semibold">Mr. Shab Uddin</span>
                                            <span class="text-gray">Commented On Your <span class="text-dark">Task</span></span>
                                        </span>
                                        <span class="sub-title">1.2 hours ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="thumb-img bg-info">
                                        <span class="text-white"><i class="fa fa-male"></i></span>
                                        {{-- <span class="text-white"><i class="fa fa-female"></i></span> --}}
                                    </span>
                                    <div class="info">
                                        <span class="title">
                                            <span class="font-size-14 text-semibold">Male Client</span>
                                            <span class="text-gray">Cancelled <span class="text-dark">Project_Name</span></span>
                                        </span>
                                        <span class="sub-title">8 hours ago</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="thumb-img bg-success">
                                        <span class="text-white"><i class="ti-target"></i></span>
                                    </span>
                                    <div class="info">
                                        <span class="title">
                                            <span class="font-size-14 text-semibold">Project_Manager</span>
                                            <span class="text-gray">Assigned You in a New <span class="text-dark">Project_Name</span></span>
                                        </span>
                                        <span class="sub-title">1 hours ago</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="notice-footer">
                        <span>
                            <a href="#" class="text-gray">Check all notifications <i class="ei-right-chevron pdd-left-5 font-size-10"></i></a>
                        </span>
                    </li>
                </ul>
            </li>

            {{-- New Messages --}}
            <li class="notifications message dropdown pdd-right-10 pdd-left-5" style="border-right: 1px solid #efefef;">
                <span class="counter mrg-right-10">01</span>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="ti-email"></i>
                </a>

                <ul class="dropdown-menu">
                    <li class="notice-header">
                        <i class="ti-email pdd-right-10"></i>
                        <span>Messages</span>
                    </li>
                    <li>
                        <ul class="list-info overflow-y-auto relative scrollable ps-container ps-theme-default ps-active-y" data-ps-id="8964a8c7-94b9-3a1f-b6b9-c6f2dc7d257c">
                            <li>
                                <a href="#">
                                    <img class="thumb-img" src="{{ asset('assets/images/avatars/thumb-5.jpg') }}" alt="">
                                    <div class="info">
                                        <span class="title">
                                            <span class="font-size-14 text-semibold">Mrs. Ahad Ullah Shah</span>
                                        <span class="text-gray">Messaged <span class="text-dark">You</span></span>
                                        </span>
                                        <span class="sub-title">5 mins ago</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="notice-footer">
                        <span>
                            <a href="#" class="text-gray">Check All Messages <i class="ei-right-chevron pdd-left-5 font-size-10"></i></a>
                        </span>
                    </li>
                </ul>
            </li>

            {{-- User Profile --}}
            <li class="user-profile dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img class="profile-img img-fluid" src="{{ asset('assets/images/user.jpg') }}" alt="">
                    <div class="user-info">
                        <span class="name pdd-right-5">{{ Auth::user()->name }}</span>
                        <i class="ti-angle-down font-size-10"></i>
                    </div>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ route('client.profile.index') }}">
                            <i class="ti-user pdd-right-10"></i>
                            <span>Profile</span>
                        </a>
                    </li>
                    {{-- <li>
                        <a href="#">
                            <i class="ti-settings pdd-right-10"></i>
                            <span>Setting</span>
                        </a>
                    </li> --}}
                    <li role="separator" class="divider"></li>
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="ti-power-off pdd-right-10"></i>
                            <span>Logout</span>
                        </a>
                    </li>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </li>
        </ul>
    </div>
</div>
