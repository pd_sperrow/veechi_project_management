<script src="{{ asset('assets/js/vendor.js') }}"></script>

@yield('script_links')

<script src="{{ asset('assets/js/app.min.js') }}"></script>

<script src="{{ asset('assets/js/toastr.min.js') }}"></script>

<script src="{{ asset('assets/js/script.js') }}"></script>
<script src="{{ asset('assets/js/responsive.js') }}"></script>

@if (Session::has('success'))
<script>
    var msg = "{{ Session::get('success') }}";
    toastr.success(msg);
</script>
@endif

@if (Session::has('error'))
<script>
    var msg = "{{ Session::get('error') }}";
    toastr.error(msg);
</script>
@endif

@if (Session::has('info'))
<script>
    var msg = "{{ Session::get('info') }}";
    toastr.info(msg);
</script>
@endif


@yield('custom_script')
