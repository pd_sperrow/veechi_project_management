<!-- Footer START -->
<footer class="content-footer">
    <div class="footer">
        <div class="copyright go-right">
            <span>Copyright © 2019 <b class="text-info">Veechi Project Management</b>. All rights reserved || Developed By <b><a href="https://veechitechnologies.com" class="text-danger" target="_blank">Veechi Technologies</a></b></span>
            {{-- <span class="go-right">
                <a href="#" class="text-primary mrg-right-10">Term &amp; Conditions</a>
                <a class="mrg-right-10">||</a>
                <a href="#" class="text-primary">Privacy &amp; Policy</a>
            </span> --}}
        </div>
    </div>
</footer>
<!-- Footer END -->
