@extends('layouts.developer.app')

@section('page_title', 'Tasks | Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .responsive-image{
            border-radius: 5px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 80px;
            width: 100%;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .p-t-8{
            padding-top: 8px !important;
        }
        .p-7{
            padding: 7px 10px !important;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>Task_Name</h4>
    </div>

    <div class="row">
        <div class="col-lg-7">
            <div class="card">
                <div class="card-heading p-b-10">
                    <h4 class="card-title"><b>Task 01</b></h4>
                </div>
                <hr class="m-t-0">
                <div class="card-block p-t-10">
                    <div class="card">
                        <div class="card-block">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sit necessitatibus quisquam labore? Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur nobis sunt cum perferendis nam repellat obcaecati inventore fugit earum quis deserunt veniam, explicabo cumque rerum reiciendis incidunt maiores quasi quo? Nostrum repudiandae fugit quas sint neque, magnam et odit, impedit soluta aliquid veritatis fugiat, in assumenda officia earum ea sequi?
                        </div>


                        <div class="row p-40 p-t-0 p-b-0">
                            {{-- Image Type --}}
                            <div class="col-md-2 p-l-5 p-r-5 task-image">
                                <a href="#">
                                    <div class="card">
                                        <div class="card-block p-0">
                                            <div class="responsive-image" style="background-image: url('{{ asset('assets/images/others/img-22.jpg') }}');"></div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            {{-- Image Type --}}
                            <div class="col-md-2 p-l-5 p-r-5">
                                <a href="#">
                                    <div class="card align-self-center" style="background: #ededed;">
                                        <div class="card-block task-file p-0 align-self-center text-center font-size-26">
                                            <b class="text-dark">File</b> <br>
                                            <i class="ti-download text-dark"></i>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                   <div class="card">
                       <div class="card-block">
                            {{-- Comments --}}
                            <ul class="list-unstyled list-info">
                                <li>
                                    <div class="pdd-vertical-10 pdd-horizon-20">
                                        <div class="portlet">
                                            <ul class="portlet-item navbar">
                                                <li>
                                                    <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle p-0" onclick="return confirm('Are you sure want to delete this Comment...?');">
                                                        <i class="ti-trash text-danger font-size-16"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <img class="thumb-img img-circle" src="{{ asset('assets/images/avatars/thumb-1.jpg') }}" alt="">
                                        <div class="info">
                                            <span class="title"><b>Jordan Hurst</b></span>
                                            <span class="sub-title">
                                                    <i class="ti-timer pdd-right-5"></i>
                                                    <span>2 min ago</span>
                                            </span>
                                        </div>
                                    </div>
                                </li>
                                <li class="p-20 p-t-10">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil similique delectus ducimus quas neque facere dolores corrupti officia laborum voluptate!</p>
                                </li>
                            </ul>
                            <hr>


                            {{-- Comment Create --}}
                            <div class="card-block p-0 text-right">
                                <form action="#" method="post">
                                    @csrf
                                    <textarea class="form-control text-left" name="comment" id="comment" placeholder="Write Your Comment..." rows="3" required></textarea>
                                    <button type="submit" class="btn btn-info btn-sm m-t-10 m-b-0">COMMENT</button>
                                </form>
                            </div>
                       </div>
                   </div>
                </div>
            </div>
        </div>

        <div class="col-lg-5">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="portlet">
                            <ul class="portlet-item navbar padding-10">
                                <li>
                                    {{-- Mark Task As Complete --}}
                                    <button type="button" class="btn btn-info btn-xs p-7" data-toggle="tooltip" data-html="true" data-placement="top" title="Mark as <b class='text-success'>Complete</b>" onclick="return confirm('Are You Sure Want to Mark This Task as Complete...?');">
                                        <i class="ti-check font-size-16"></i>
                                    </button>

                                    {{-- Start Working On this Task --}}
                                    <button type="button" class="btn btn-success btn-xs p-7" data-toggle="tooltip" data-html="true" data-placement="top" title="<b class='text-success'>Start</b> Work">
                                        <i class="ti-timer font-size-16 text-bold"></i>
                                    </button>

                                    {{-- Stop Working On this Task --}}
                                    {{-- <button type="button" class="btn btn-danger btn-xs p-7" data-toggle="tooltip" data-html="true" data-placement="top" title="<b class='text-danger'>Stop</b> Work">
                                        <i class="ti-alarm-clock font-size-16"></i>
                                    </button> --}}
                                </li>
                            </ul>
                        </div>
                        <div class="card-heading p-b-10">
                            <h4 class="card-title"><b>Task Overview</b></h4>
                        </div>
                        <hr class="m-t-0">
                        <div class="card-block p-t-10">
                            <table class="table table-bordered table-responsive no-mrg-btm">
                                <tbody>
                                    <tr>
                                        <th>Project</th>
                                        <td><a href="#" class="text-bold text-info">Project_Name</a></td>
                                    </tr>
                                    <tr>
                                        <th>Task Assigned By</th>
                                        <td class="text-bold text-dark">Project_Manager_Name</td>
                                    </tr>
                                    <tr>
                                        <th>Task Assigned</th>
                                        <td class="text-bold text-primary">10th Nov, 2019</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td class="text-bold text-dark">In Progress</td>
                                    </tr>
                                    <tr>
                                        <th>Task Deadline</th>
                                        <td class="text-bold text-danger">15th Nov, 2019</td>
                                    </tr>
                                    <tr>
                                        <th>Task Start</th>
                                        <td class="text-bold text-info">10th Nov, 2019</td>
                                    </tr>
                                    <tr>
                                        <th>Task Complete</th>
                                        <td class="text-bold text-success">14th Nov, 2019</td>
                                    </tr>
                                    <tr>
                                        <th>Task Members</th>
                                        <td>
                                            <ul class="list-info">
                                                <li>
                                                    <a href="#" class="p-0">
                                                        <img class="thumb-img img-circle m-l-2 m-r-2" src="{{ asset('assets/images/avatars/thumb-1.jpg') }}" data-toggle="tooltip" data-html="true" data-placement="top" title="Developer_Name">
                                                    </a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-heading p-b-10">
                            <h4 class="card-title"><b>My Timesheet For This Task</b></h4>
                        </div>
                        <hr class="m-t-0">
                        <div class="card-block p-t-10">
                            <table class="table table-bordered table-responsive no-mrg-btm">
                                <thead>
                                    <tr>
                                        <th class="text-center">Sl.</th>
                                        <th class="text-center">Date</th>
                                        <th class="text-center">Start Time</th>
                                        <th class="text-center">Stop Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">01</td>
                                        <td class="text-center">11th Oct, 2019</td>
                                        <td class="text-center">11:15 AM</td>
                                        <td class="text-center">06:45 PM</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="portlet">
                            <ul class="portlet-item navbar">
                                <li>
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#upload_file" class="btn btn-icon btn-flat">
                                        <i class="ti-upload"></i>
                                        <b>Upload File</b>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-heading p-b-10">
                            <h4 class="card-title"><b>My Task Files</b></h4>
                        </div>
                        <hr class="m-t-0">
                        <div class="card-block p-t-10">
                            <table class="table table-bordered table-responsive no-mrg-btm">
                                <thead>
                                    <tr>
                                        <th>Sl.</th>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>01</td>
                                        <td><b><a href="#">project-description</a></b></td>
                                        <td>11th Oct, 2019</td>
                                        <td class="text-center">
                                            <div class="dropdown">
                                                <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle p-0" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#view_file_details">
                                                            <i class="ti-eye pdd-right-10 text-info"></i>
                                                            <span>View</span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#">
                                                            <i class="ti-download pdd-right-10 text-success"></i>
                                                            <span>Download</span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#" onclick="return confirm('Are You Sure Want To Delete This File..?');">
                                                            <i class="ti-close pdd-right-10 text-danger"></i>
                                                            <span>Delete</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('developer.project.modals')
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

    </script>
@endsection
