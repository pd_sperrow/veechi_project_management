{{-- Upload File Modal --}}
<div class="modal fade" id="upload_file">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="border btm padding-15">
                <h4 class="no-mrg">Upload File</h4>
            </div>
            <div class="modal-body">
                <form action="#" method="post">
                    @csrf
                    <div class="form-group">
                        <label>Select Files</label>
                        <input type="file" multiple data-show-upload="true" data-show-caption="true" class="form-control file" name="file[]" id="file" placeholder="Event Title" required>
                        <small><q><b class="text-info">Note:</b> You can select <span class="text-danger">One or Multiple</span> Files.</q></small>
                    </div>
                    <div class="form-group">
                        <label>Short Note</label>
                        <textarea class="form-control" name="note" id="note" placeholder="Short Note"></textarea>
                    </div>
                    <div class="text-right">
                        <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-success btn-sm" type="submit">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>




{{-- View File Details Modal --}}
<div class="modal fade" id="view_file_details">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="border btm padding-15">
                <h4 class="no-mrg">file_name</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered no-mrg-btm">
                    <tbody>
                        <tr>
                            <th style="width: 30%;">File Name</th>
                            <td style="width: 70%;">project_description</td>
                        </tr>
                        <tr>
                            <th>File Size</th>
                            <td>102kb</td>
                        </tr>
                        <tr>
                            <th>Upload Date</th>
                            <td>11th Oct, 2019</td>
                        </tr>
                        <tr>
                            <th>Upload Time</th>
                            <td>11:00 AM</td>
                        </tr>
                        <tr>
                            <th>Note</th>
                            <td>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eos molestiae tempore laudantium quasi voluptate, harum veniam error quisquam laboriosam doloribus.</td>
                        </tr>
                    </tbody>
                </table>
                <div class="text-right m-t-20">
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
