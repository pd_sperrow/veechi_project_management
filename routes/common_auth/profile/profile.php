<?php

// Profile Routes
Route::group([
    'prefix' => 'profile', //URL
    'as' => 'profile.', //Route
    'namespace' => 'Profile', // Controller
],
    function(){
        Route::post('/updateProfile', 'ProfileController@updatePassword')->name('update.password');
    }
);
