<?php

// common Routes
Route::group([
    'prefix' => 'common_auth', // URL
    'as' => 'common_auth.', // Route
    'namespace' => 'CommonAuth', // Controller
],
    function(){
        // Profile
        include_once 'profile/profile.php';
    }
);
