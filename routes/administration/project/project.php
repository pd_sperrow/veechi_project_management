<?php

// Administration Projects Routes
Route::group([
    'prefix' => 'project', //URL
    'as' => 'project.', //Route
    'namespace' => 'Project', // Controller
],
    function(){
        Route::get('/all_project', 'ProjectController@all_project')->name('all_project');

        Route::get('/finished', 'ProjectController@finished')->name('finished');

        Route::get('/awaiting_feedback', 'ProjectController@awaiting_feedback')->name('awaiting_feedback');

        Route::get('/testing', 'ProjectController@testing')->name('testing');

        Route::get('/in_progress', 'ProjectController@in_progress')->name('in_progress');

        Route::get('/on_hold', 'ProjectController@on_hold')->name('on_hold');

        Route::get('/not_started', 'ProjectController@not_started')->name('not_started');

        Route::get('/cancelled', 'ProjectController@cancelled')->name('cancelled');

        Route::get('/show/{project_id}', 'ProjectController@show')->name('show');
    }
);
