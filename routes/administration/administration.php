<?php

// Administration Routes
Route::group([
    'prefix' => 'administration', // URL
    'as' => 'administration.', // Route
    'namespace' => 'Administration', // Controller
],
    function(){
        // Dashboard
        include_once 'dashboard/dashboard.php';
        // Profile
        include_once 'profile/profile.php';
        // Project
        include_once 'project/project.php';
        // Task
        include_once 'task/task.php';
        // Invoice
        include_once 'invoice/invoice.php';
    }
);
