<?php

// Administration Invoice Routes
Route::group([
    'prefix' => 'invoice', //URL
    'as' => 'invoice.', //Route
    'namespace' => 'Invoice', // Controller
],
    function(){
        Route::get('/all_invoice', 'InvoiceController@all_invoice')->name('all_invoice');

        Route::get('/paid', 'InvoiceController@paid')->name('paid');

        Route::get('/unpaid', 'InvoiceController@unpaid')->name('unpaid');

        Route::get('/overdue', 'InvoiceController@overdue')->name('overdue');

        Route::get('/cancelled', 'InvoiceController@cancelled')->name('cancelled');

        Route::get('/draft', 'InvoiceController@draft')->name('draft');

        Route::get('/create', 'InvoiceController@create')->name('create');
    }
);
