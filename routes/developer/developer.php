<?php

// developer Routes
Route::group([
    'prefix' => 'developer', // URL
    'as' => 'developer.', // Route
    'namespace' => 'Developer', // Controller
],
    function(){
        // Dashboard
        include_once 'dashboard/dashboard.php';
        // Profile
        include_once 'profile/profile.php';
        // Project
        include_once 'project/project.php';
        // Task
        include_once 'task/task.php';
        // Support
            // FAQ
            include_once 'support/faq/faq.php';
            // Contact
            include_once 'support/contact/contact.php';
    }
);
