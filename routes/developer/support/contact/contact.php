<?php

// Developer Support Routes
Route::group([
    'prefix' => 'support', //URL
    'as' => 'support.', //Route
    'namespace' => 'Support', // Controller
],
    function(){
        Route::get('/contact', 'SupportController@contact_index')->name('contact_index');

        Route::get('/contact/create', 'SupportController@contact_create')->name('contact_create');
    }
);
