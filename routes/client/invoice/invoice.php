<?php

// Client Invoice Routes
Route::group([
    'prefix' => 'invoice', //URL
    'as' => 'invoice.', //Route
    'namespace' => 'Invoice', // Controller
],
    function(){
        Route::get('/', 'InvoiceController@index')->name('index');
        Route::get('/show/{invoice_id}/{id}', 'InvoiceController@show')->name('show');
    }
);
