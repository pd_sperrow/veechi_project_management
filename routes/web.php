<?php

Auth::routes();

Route::get('/','HomeController@index');
Route::post('/loginwithuser', 'Auth\LoginController@loginWithEmailOrUsername')->name('loginWithUser');

/*===================================
===========< auth Routes >==========
all kind of role has been agreed in this route
===================================*/
Route::group(
    [
        'middleware' => ['auth'],
    ],
    function () {
        include_once 'common_auth/common_auth.php';
    }
);



/*===================================
===========< SuperAdmin Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'superadmin'],
    ],
    function () {
        include_once 'superadmin/superadmin.php';
    }
);


/*===================================
===========< Administration Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'administration'],
    ],
    function () {
        include_once 'administration/administration.php';
    }
);


/*===================================
===========< Finance Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'finance'],
    ],
    function () {
        include_once 'finance/finance.php';
    }
);



/*===================================
===========< Project Manager Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'projectmanager'],
    ],
    function () {
        include_once 'projectmanager/projectmanager.php';
    }
);


/*===================================
===========< Developer Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'developer'],
    ],
    function () {
        include_once 'developer/developer.php';
    }
);


/*===================================
===========< Client Routes >==========
===================================*/
Route::group(
    [
        'middleware' => ['auth', 'client'],
    ],
    function () {
        include_once 'client/client.php';
    }
);


