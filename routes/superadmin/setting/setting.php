<?php

// Super Admin Setting Routes
Route::group([
    'prefix' => 'setting', //URL
    'as' => 'setting.', //Route
    'namespace' => 'Setting', // Controller
],
    function(){
        Route::get('/superadmin', 'SettingController@superadmin')->name('superadmin');

        Route::get('/administration', 'SettingController@administration')->name('administration');

        Route::get('/finance', 'SettingController@finance')->name('finance');

        Route::get('/project_manager', 'SettingController@project_manager')->name('project_manager');

        Route::get('/employee', 'SettingController@employee')->name('employee');


        Route::post('/create_user/{role}', 'SettingController@create_user')->name('create_user');
    }
);
