<?php

namespace App\Http\Controllers\Developer\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //developer dashboard index
    public function index(){
        return view('developer.dashboard.index');
    }
}
