<?php

namespace App\Http\Controllers\Developer\Support;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class SupportController extends Controller
{
    public function faq_index()
    {
        return view('developer.support.faq.index');
    }


    public function contact_index()
    {
        return view('developer.support.contact.index');
    }


    public function contact_create()
    {
        return view('developer.support.contact.create');
    }
}
