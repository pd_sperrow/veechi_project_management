<?php

namespace App\Http\Controllers\ProjectManager\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $profile = User::find(Auth::user()->id);
        // dd($profile);
        return view('projectmanager.profile.index', compact(['profile']));
    }
}
