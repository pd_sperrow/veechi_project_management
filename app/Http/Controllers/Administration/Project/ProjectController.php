<?php

namespace App\Http\Controllers\Administration\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    public function all_project()
    {
        return view('administration.project.all_project');
    }

    public function finished()
    {
        return view('administration.project.finished');
    }

    public function awaiting_feedback()
    {
        return view('administration.project.awaiting_feedback');
    }

    public function testing()
    {
        return view('administration.project.testing');
    }

    public function in_progress()
    {
        return view('administration.project.in_progress');
    }

    public function on_hold()
    {
        return view('administration.project.on_hold');
    }

    public function not_started()
    {
        return view('administration.project.not_started');
    }

    public function cancelled()
    {
        return view('administration.project.cancelled');
    }

    public function show()
    {
        return view('administration.project.show');
    }
}
