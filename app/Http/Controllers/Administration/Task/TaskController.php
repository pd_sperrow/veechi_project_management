<?php

namespace App\Http\Controllers\Administration\Task;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public function all_task()
    {
        return view('administration.task.all_task');
    }

    public function finished()
    {
        return view('administration.task.finished');
    }

    public function awaiting_feedback()
    {
        return view('administration.task.awaiting_feedback');
    }

    public function testing()
    {
        return view('administration.task.testing');
    }

    public function in_progress()
    {
        return view('administration.task.in_progress');
    }

    public function on_hold()
    {
        return view('administration.task.on_hold');
    }

    public function not_started()
    {
        return view('administration.task.not_started');
    }

    public function cancelled()
    {
        return view('administration.task.cancelled');
    }

    public function show()
    {
        return view('administration.task.show');
    }
}
