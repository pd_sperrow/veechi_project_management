<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        // dd($request);
        if (Auth::guard($guard)->check() && Auth::user()->role->id == 1) {
            return redirect()->route('superadmin.dashboard.index');
        } elseif (Auth::guard($guard)->check() && Auth::user()->role->id == 2) {
            return redirect()->route('administration.dashboard.index');
        } elseif (Auth::guard($guard)->check() && Auth::user()->role->id == 3) {
            return redirect()->route('finance.dashboard.index');
        } elseif (Auth::guard($guard)->check() && Auth::user()->role->id == 4) {
            return redirect()->route('projectmanager.dashboard.index');
        } elseif (Auth::guard($guard)->check() && Auth::user()->role->id == 5) {
            return redirect()->route('developer.dashboard.index');
        } elseif (Auth::guard($guard)->check() && Auth::user()->role->id == 6) {
            return redirect()->route('client.dashboard.index');
        } else{
            return $next($request);
        }
    }
}
