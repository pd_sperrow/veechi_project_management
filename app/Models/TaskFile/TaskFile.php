<?php

namespace App\Models\TaskFile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskFile extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'task_id', 'file',
    ];

    /**
     * this function for taskUser relations
     * @var function
     *
     */
    public function taskUser(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * this function for task relations
     * @var function
     *
     */
    public function task(){
        return $this->belongsTo('App\Models\Task\Task', 'task_id', 'id');
    }
}
