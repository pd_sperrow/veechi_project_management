<?php

namespace App\Models\Invoice;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'provider_id', 'invoice_number', 'invoice_date', 'expire_date', 'tax', 'total', 'sub_total', 'currency', 'discount_type', 'discount', 'payable_amount',
    ];

    /**
     * this function for client relations
     * @var function
     *
     */
    public function client(){
        return $this->belongsTo('App\User', 'client_id', 'id');
    }

    /**
     * this function for provider relations
     * @var function
     *
     */
    public function provider(){
        return $this->belongsTo('App\User', 'provider_id', 'id');
    }

    /**
     * this function for project relations
     * @var function
     *
     */
    public function project(){
        return $this->belongsTo('App\Models\Project\Project', 'project_id', 'id');
    }

    /**
     * this function for hosting relations
     * @var function
     *
     */
    public function hosting(){
        return $this->belongsTo('App\Models\Hosting\Hosting', 'hosting_id', 'id');
    }


    // ============================= < Relation Depends on Invoice >========================

    /**
     * this function for invoiceCarts relations
     * @var function
     *
     */
    public function invoiceCarts(){
        return $this->hasMany('App\Models\InvoiceCart\InvoiceCart', 'invoice_id', 'id');
    }

    /**
     * this function for paymentReport relations
     * @var function
     *
     */
    public function paymentReport(){
        return $this->hasOne('App\Models\PaymentReport\PaymentReport', 'invoice_id', 'id');
    }
}
