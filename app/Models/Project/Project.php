<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'name', 'project_type', 'description',
    ];


    /**
     * this function for client relations
     * @var function
     *
     */
    public function client(){
        return $this->belongsTo('App\User', 'client_id', 'id');
    }


    /**
     * this function for project manager relations
     * @var function
     *
     */
    public function project_manager(){
        return $this->belongsTo('App\User', 'project_manager_id', 'id');
    }


    /**
     * this function for receiver relations
     * @var function
     *
     */
    public function project_receiver(){
        return $this->belongsTo('App\User', 'receiver_id', 'id');
    }


    // ============================= < Relation Depends on Project >========================

    /**
     * this function for projectDevelopers relations
     * @var function
     *
     */
    public function projectDevelopers(){
        return $this->hasMany('App\Models\AssignedDeveloper\AssignedDeveloper', 'project_id', 'id');
    }

    /**
     * this function for projectComments relations
     * @var function
     *
     */
    public function projectComments(){
        return $this->hasMany('App\Models\Comment\Comment', 'project_id', 'id');
    }

    /**
     * this function for projectInvoices relations
     * @var function
     *
     */
    public function projectInvoices(){
        return $this->hasMany('App\Models\Invoice\Invoice', 'project_id', 'id');
    }

    /**
     * this function for projectTasks relations
     * @var function
     *
     */
    public function projectTasks(){
        return $this->hasMany('App\Models\Task\Task', 'project_id', 'id');
    }

    /**
     * this function for projectReports relations
     * @var function
     *
     */
    public function projectReports(){
        return $this->hasMany('App\Models\ProjectReport\ProjectReport', 'project_id', 'id');
    }

    /**
     * this function for paymentReports relations
     * @var function
     *
     */
    public function paymentReports(){
        return $this->hasMany('App\Models\PaymentReport\PaymentReport', 'project_id', 'id');
    }

    /**
     * this function for projectFiles relations
     * @var function
     *
     */
    public function projectFiles(){
        return $this->hasMany('App\Models\ProjectFile\ProjectFile', 'project_id', 'id');
    }
}
