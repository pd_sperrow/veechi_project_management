<?php

namespace App\Models\AssignedDeveloper;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignedDeveloper extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'developer_id', 'project_id', 'access_to_comment',
    ];

    /**
     * this function for developer_assigned relations
     * (Who is the developer of this project)
     * @var function
     *
     */
    public function developerAssigned(){
        return $this->belongsTo('App\User', 'developer_id', 'id');
    }

    /**
     * this function for project relations
     * @var function
     *
     */
    public function project(){
        return $this->belongsTo('App\Models\Project\Project', 'project_id', 'id');
    }



}
