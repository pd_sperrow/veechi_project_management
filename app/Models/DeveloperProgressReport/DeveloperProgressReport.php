<?php

namespace App\Models\DeveloperProgressReport;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeveloperProgressReport extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'task_id', 'developer_id', 'received_point',
    ];

    /**
     * this function for developer_progress relations
     * @var function
     *
     */
    public function developerProgress(){
        return $this->belongsTo('App\User', 'developer_id', 'id');
    }

    /**
     * this function for task relations
     * @var function
     *
     */
    public function task(){
        return $this->belongsTo('App\Models\Task\Task', 'task_id', 'id');
    }
}
