<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domains', function (Blueprint $table) {
            $table->bigIncrements('id');

            //foreign key for client
            $table->bigInteger('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');

            //foreign key, who create the task
            $table->bigInteger('creator_id')->unsigned();
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('cascade');

            //foreign key, hosting_id for which project
            $table->bigInteger('hosting_id')->unsigned()->nullable();
            $table->foreign('hosting_id')->references('id')->on('projects')->onDelete('cascade');

            $table->string('domain_url');
            $table->string('domain_purchase_url');
            $table->string('username');
            $table->string('password');
            $table->text('note')->nullable();
            $table->date('purchase_date')->nullable();
            $table->date('expire_date')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domains');

        Schema::table("domains", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
