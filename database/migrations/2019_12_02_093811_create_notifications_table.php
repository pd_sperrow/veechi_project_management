<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');

            //foreign key for client
            $table->bigInteger('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');

            //foreign key for from
            $table->bigInteger('from_id')->unsigned()->nullable();
            $table->foreign('from_id')->references('id')->on('users')->onDelete('cascade');


            //foreign key for to
            $table->bigInteger('to_id')->unsigned();
            $table->foreign('to_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('type', 60)->nullable();
            $table->morphs('notifiable');
            $table->text('notification');
            $table->timestamp('read_at')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');

        Schema::table("notifications", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
